import {createStackNavigator, createAppContainer} from 'react-navigation';
import LoginPage from './LoginPage';
import Status from './Status';

const AppNavigator = createStackNavigator({
  Home: {screen: LoginPage},
  Stats: {screen: Status},
}
);

const App = createAppContainer(AppNavigator);

export default App;