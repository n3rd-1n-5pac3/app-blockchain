import React from 'react';
import { StyleSheet, View, TextInput, Text, Button, Image } from 'react-native';

export default class LoginPage extends React.Component {
    static navigationOptions = { header: null } 

    constructor(props) {
        super(props);
        this.state = { text: 'sdasd' };
    }
    
    render() {
        return (
          
          <View style={styles.container}>
            <View style={styles.image}> 
              <Image source = './vitay.png'/>
            </View>
          
          <Text>User ID: </Text>
          <TextInput
            style={{height: 40, borderColor: 'gray', borderWidth: 1}}
            onChangeText={(text) => this.setState({text: text})}
            value={this.state.text}
          />
          <Button
            title="Login"
            onPress={() => this.props.navigation.navigate('Stats', {userId: this.state.text})}
          />
          </View>
        );
      }
    }
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        flexDirection: 'column',
      },
      image: {
        alignItems: 'center',
        justifyContent:'center',
      }
    });