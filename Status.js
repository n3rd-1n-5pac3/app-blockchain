import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';

export default class Status extends React.Component {
  static navigationOptions = { header: null } 

  constructor(props) {
    super(props);
    this.state = { 
      userId: this.props.navigation.getParam('userId', 'Not found')
    };
}

  render() {
    return (
      <View style={styles.container}>
        <Text>User ID: </Text>
        <Text>{this.state.userId}</Text>
        <Text>Number of points: </Text>
        <Text>123</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffedd1',
    flexDirection: 'column',
  },

  row: {
    flex: 1, 
    flexDirection: 'column',
    backgroundColor: '#ffedd1',
  },
});